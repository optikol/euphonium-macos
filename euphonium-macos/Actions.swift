import Foundation

protocol JsonSerializable {
    func serialize() -> String
}

enum Method: JsonSerializable {
    case connect
    case insertAtPt

    func serialize() -> String {
        switch self {
        case .connect:
            return "connect"
        case .insertAtPt:
            return "insertAtPt"
        }
    }
}

class ConnectReq: JsonSerializable {
    var clientId: String
    var method: Method

    init(clientId: String, method: Method) {
        self.clientId = clientId
        self.method = method
    }

    func serialize() -> String {
        let dict: [String: Any] = [
            "clientId": clientId,
            "method": method.serialize()
        ]
        return try!(String(data: JSONSerialization.data(withJSONObject: dict, options: []),
                           encoding: .utf8))!
    }
}
