import Foundation

class Editor {
    var clientId: String
    var serverId: String?
    var buffer: Buffer

    init(clientId: String, serverId: String?) {
        self.clientId = clientId
        self.serverId = serverId
        buffer = Buffer()
    }
}

struct Line {
    var line: String
    var number: Int
}

struct Point {
    var r: Int
    var c: Int
}

class Buffer {
    var lines: [Line]
    var textLen: Int
    var point: Point

    init() {
        lines = []
        textLen = 0
        point = Point(r: 0, c: 0)
    }
}
